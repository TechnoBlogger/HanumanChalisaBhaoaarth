package com.god.hanuman.chalisa;

import android.app.Application;
import android.support.v4.app.FragmentActivity;

import com.god.hanuman.chalisa.utils.FlowOrganizer;


/**
 * Created by Techno Blogger on 12/05/16.
 */

public class SingletonApp extends Application {
    private FragmentActivity _activity;
    private FlowOrganizer _flow;
    private static SingletonApp _app;
    private MainActivity _activityMain;

    public void onDestroy() {
        _activity = null;
        _flow = null;
    }

    public void initActivity(FragmentActivity _activity) {
        this._activity = _activity;
    }

    public FragmentActivity getFragmentActivityInstance() {
        return _activity;
    }

    public MainActivity getActivityInstanse() {
        return _activityMain;
    }

    public static SingletonApp getApp() {
        if (_app == null)
            _app = new SingletonApp();
        return _app;
    }

    public void initFlowManager(int idParentFameView, FragmentActivity _activity) {
        _flow = new FlowOrganizer(_activity, idParentFameView);
    }

    public FlowOrganizer getFlowInstanse() {
        if (_flow == null) {
            _flow = new FlowOrganizer(_activity, R.id.frame_container);
        }
        return _flow;
    }
}