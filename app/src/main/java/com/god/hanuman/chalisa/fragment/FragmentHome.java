package com.god.hanuman.chalisa.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.god.hanuman.chalisa.R;
import com.god.hanuman.chalisa.SingletonApp;

/**
 * Created by aman on 7/6/16.
 */
public class FragmentHome extends Fragment implements View.OnClickListener {

    private Button btnChalisa, btnDoha, btnChalisaArth, btnAarti, btnMantra;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, null);

        btnChalisa = (Button) rootView.findViewById(R.id.btnChalisa);
        btnDoha = (Button) rootView.findViewById(R.id.btnDoha);
        btnChalisaArth = (Button) rootView.findViewById(R.id.btnChalisaArth);
        btnAarti = (Button) rootView.findViewById(R.id.btnAarti);
        btnMantra = (Button) rootView.findViewById(R.id.btnMantra);

        btnChalisa.setOnClickListener(this);
        btnDoha.setOnClickListener(this);
        btnChalisaArth.setOnClickListener(this);
        btnAarti.setOnClickListener(this);
        btnMantra.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChalisa:
                SingletonApp.getApp().getFlowInstanse().replace(new FragmentChalisa(), true);
                break;
            case R.id.btnDoha:
                SingletonApp.getApp().getFlowInstanse().replace(new FragmentDoha(), true);
                break;
            case R.id.btnChalisaArth:
                SingletonApp.getApp().getFlowInstanse().replace(new FragmentChalisaAarth(), true);
                break;
            case R.id.btnAarti:
                SingletonApp.getApp().getFlowInstanse().replace(new FragmentAarti(), true);
                break;
            case R.id.btnMantra:
                SingletonApp.getApp().getFlowInstanse().replace(new FragmentMantra(), true);
                break;
            default:
                break;
        }

    }
}