package com.god.hanuman.chalisa.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.god.hanuman.chalisa.R;
import com.god.hanuman.chalisa.interfaces.IAllConstants;

/**
 * Created by aman on 7/6/16.
 */
public class FragmentAbout extends Fragment {
    private TextView txtChalisa;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, null);
        txtChalisa = (TextView) rootView.findViewById(R.id.textAbout);
        txtChalisa.setText(IAllConstants.Messages.About);
        return rootView;
    }
}
