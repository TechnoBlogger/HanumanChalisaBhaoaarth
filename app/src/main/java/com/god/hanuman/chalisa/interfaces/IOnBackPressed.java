package com.god.hanuman.chalisa.interfaces;


/**
 * Created by TechnoBlogger on 27/1/16.
 */

public interface IOnBackPressed {
	public boolean onBackPressed();

}
