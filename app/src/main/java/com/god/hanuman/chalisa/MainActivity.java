package com.god.hanuman.chalisa;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.appbrain.AppBrain;
import com.appbrain.InterstitialBuilder;
import com.god.hanuman.chalisa.fragment.FragmentAbout;
import com.god.hanuman.chalisa.fragment.FragmentHome;
import com.god.hanuman.chalisa.fragment.FragmentName;
import com.god.hanuman.chalisa.interfaces.IAllConstants;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.List;

public class MainActivity extends FragmentActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private int _backBtnCount = 0;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppBrain.init(this);
        if (savedInstanceState == null) {
            InterstitialBuilder.create().show(this);
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(IAllConstants.Messages.Title);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final String appPackageName = getPackageName();
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Hanuman Chaalisa");
                    String sAux = "\nLet me recommend you this application\n\n";
                    sAux = sAux + ("https://play.google.com/store/apps/details?id=" + appPackageName);
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "Select One"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        SingletonApp.getApp().initActivity(this);
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SingletonApp.getApp().getFlowInstanse().add(new FragmentHome(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (getSupportFragmentManager() != null) {
            List<Fragment> list = getSupportFragmentManager().getFragments();
            if (list == null)
                return;
            for (Fragment f : list) {
                if (f != null)
                    f.onLowMemory();
            }
        }
    }

/*

    @Override
    public void onBackPressed() {

        boolean isbaseBackPress = true;

        SingletonApp.getApp();
        if (SingletonApp.getApp().getFlowInstanse() != null) {
            List<Fragment> list = getSupportFragmentManager().getFragments();
            if (list != null) {
                for (Fragment fragment : list) {
                    if (fragment instanceof BaseFragment) {
                        isbaseBackPress = ((BaseFragment) fragment).onBackPressed();
                    }
                }
            }
        }

        if (!isbaseBackPress)
            return;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                _backBtnCount = 0;
            }
        }, 1000);

        if (!SingletonApp.getApp().getFlowInstanse().hasNoMoreBack())
            super.onBackPressed();
        else {
            _backBtnCount++;
            if (_backBtnCount == 2) {
                System.exit(1);
                finish();
            } else {
                exitDialog();
            }
        }
    }
*/

    public void exitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Are You Sure you want to Exit? ");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    protected void onDestroy() {
        SingletonApp.getApp().onDestroy();
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // Home

        if (id == R.id.nav_home) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentHome(), true);
            SingletonApp.getApp().getFlowInstanse().clearBackStack();
        }

        // Names

        else if (id == R.id.nav_names) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentName(), true);
        }

        // About

        else if (id == R.id.nav_info) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentAbout(), true);
        }

        // Rate

        else if (id == R.id.nav_rate) {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.show();
            final String appPackageName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id="
                                + appPackageName)));
            }
            dialog.dismiss();

        }

        // Share

        else if (id == R.id.nav_share) {
            try {
                final String appPackageName = getPackageName();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Hanuman Chalisa ");
                String sAux = "\nLet me recommend you this application\n\n";
                sAux = sAux + ("https://play.google.com/store/apps/details?id=" + appPackageName);
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "Select One"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
