package com.god.hanuman.chalisa.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.god.hanuman.chalisa.R;
import com.god.hanuman.chalisa.interfaces.IAllConstants;

/**
 * Created by aman on 7/6/16.
 */
public class FragmentChalisa extends Fragment {
    private TextView txtChalisa;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chalisa, null);
        txtChalisa = (TextView) rootView.findViewById(R.id.textChalisa);
        txtChalisa.setText(IAllConstants.Messages.Chalisa);
        return rootView;
    }
}
