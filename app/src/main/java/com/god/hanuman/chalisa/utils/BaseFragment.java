package com.god.hanuman.chalisa.utils;


import android.support.v4.app.Fragment;

import com.god.hanuman.chalisa.interfaces.IOnBackPressed;


/**
 * Created by TechnoBlogger on 27/1/16.
 */

public class BaseFragment extends Fragment implements IOnBackPressed {

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
